# Validated Input

```javascript
import ValidatedInput from '@mtntop/validated-input';
Vue.use(ValidatedInput);
```

## Validation Rules
```
https://vee-validate.logaretm.com/v2/guide/rules.html#integer
```

## Usage example:

```vue
<validated-input label="Zip" :validate="{ required: true, regex: /\d+/ }" v-model="value.zip" />
```

## Validation modes:

- instant - whenever field appears on the page
- onfocus (default) - whenever field is clicked
- wait - wait for explicit calling of `$validator.validateAll()`

### components exported :

1. `validated-input` - validated input, select, text-area
2. `autocomplete-input` - input with list of options loaded with pagination(on scroll or search)
3. [multiselect](https://vue-multiselect.js.org/)

## Push Development Changes:

1. Run `npm version patch` - this will change version, create git tags, push git tags
1. Run `npm run build` - build the app
1. Run `npm publish --access public` - publish last build to remote repo, before publish make sure you have dupgrade version and build

### Development :

1. `npm run watch:server` - start front end dev
