import AutoCompleteInput from './components/AutoCompleteInput.vue';
import ValidatedInput from './components/ValidatedInput.vue';
const VERSION = require('../package.json').version;
import VeeValidate from 'vee-validate';
import Multiselect from 'vue-multiselect';
import MaskedInput from 'vue-masked-input';
import VueMask from 'v-mask';
import { postcodeValidator } from 'postcode-validator';
import 'vue-multiselect/dist/vue-multiselect.min.css';

export default {
  install(Vue, options) {
    //
    String.prototype.toCapitalize = function() {
      return this.charAt(0).toUpperCase() + this.slice(1).toLowerCase();
    };

    Vue.component('autocomplete-input', AutoCompleteInput);
    Vue.component('multiselect', Multiselect);

    VeeValidate.Validator.extend(
      'enum',
      {
        validate: (value, params) => {
          return params.values.includes(value);
        },
        getMessage: (field, params) => {
          return `can be only: ${params[0].join(', ')}`;
        },
      },
      {
        paramNames: ['values'],
      }
    );
    VeeValidate.Validator.extend(
      'postalCode',
      {
        validate: (value, { country }) => {
          return postcodeValidator(value, country);
        },
        getMessage: (field) => {
          return `The ${field} code is not valid for the selected country.`;
        },
      },
      {
        paramNames: ['country'],
      }
    );
    VeeValidate.Validator.extend('maxVal', {
      //standart max work only with digits
      getMessage(field, params, data) {
        return `Field should be less than ${params}`;
      },
      validate: (value, [testProp]) => {
        return parseFloat(value) <= parseFloat(testProp);
      },
    });
    VeeValidate.Validator.extend('minVal', {
      getMessage(field, params, data) {
        return `Field should be more than ${params}`;
      },
      validate: (value, [testProp]) => {
        return parseFloat(value) >= parseFloat(testProp);
      },
    });
    VeeValidate.Validator.extend('increment', {
      getMessage(field, params, data) {
        return `Field should be divisible by ${params}`;
      },
      validate: (value, [testProp]) => {
        return parseInt(value) % parseFloat(testProp) === 0;
      },
    });
    // Vue.use(VeeValidate, {
    //   inject: ['$validator'],
    //   fieldsBagName: 'veeFields'
    // });
    const dict = {
      custom: {
        Quantity: {
          min_value: (field, val) => `Please enter a quantity of ${val} or greater`,
        },
      },
    };

    VeeValidate.Validator.extend('partSizes', {
      getMessage(field, params, data) {
        return `Error`;
      },
      validate: async (value, [testProp]) => {
        return testProp === 1;
      },
    });
    VeeValidate.Validator.extend('increment', {
      getMessage(field, params, data) {
        return `Must be ordered in increments of ${params}`;
      },
      validate: (value, [testProp]) => {
        return parseInt(value) % parseFloat(testProp) === 0;
      },
    });

    Vue.use(VeeValidate, {
      inject: ['$validator'],
      fieldsBagName: 'veeFields',
    });

    Vue.component('MaskedInput', MaskedInput);
    Vue.use(VueMask);
    VeeValidate.Validator.localize('en', dict);
    Vue.component('validated-input', ValidatedInput);
    console.log('MPowerValidate Input %s', VERSION);
  },
};
export { ValidatedInput, AutoCompleteInput, Multiselect };
